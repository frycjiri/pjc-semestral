//
// Created by frycj on 25/12/2018.
//

#ifndef HW2_INPUTSETTINGS_H
#define HW2_INPUTSETTINGS_H

struct InputSettings {
public:
    float Gravity=6.67384e-20;
    float TimeFrom=0;
    float TimeTo=31536000.f*2;
    float TimeDelta=86400.f*5;
    bool StarsFixed=true;
    bool PlanetsFixed=false;
    bool SingleThread=false;
    float X1=-4.000000000000000E+09;
    float X2= 3.500000000000000E+09;
    float Y1=-4.000000000000000E+09;
    float Y2= 3.500000000000000E+09;
    unsigned int FrameWidth=800;
    unsigned int FrameHeight=800;
};
#endif //HW2_INPUTSETTINGS_H
