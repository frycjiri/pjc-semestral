//
// Created by frycj on 30/12/2018.
//

#ifndef HW2_RANDOMSETTINGS_H
#define HW2_RANDOMSETTINGS_H

#include <vector>
#include "CosmicObjects.h"
class ObjectGenerator {
private:
    int type,count=0,seed;
    float minMass,minX,minY,minZ,minVelX,minVelY,minVelZ;
    float maxMass,maxX,maxY,maxZ,maxVelX,maxVelY,maxVelZ;

    float rnd_d(float min, float max)
    {
        return ((double)rand() / RAND_MAX) * (max - min) + min;
    }
public:
    string name;
    explicit ObjectGenerator(stringstream ss)
    {
        ss >> name >> type >> count >> seed;
        ss >> minMass >> maxMass >> minX >> maxX >> minY >> maxY >> minZ >> maxZ;
        ss >> minVelX >> maxVelX >> minVelY >> maxVelY >> minVelZ >> maxVelZ;
    }

    void populate(vector<CosmicObject> &data)
    {
        srand(seed);
        for(int i=0;i<count;i++)
        {
            CosmicObject object;
            object.Type=type;
            object.Name=name;
            object.Mass=rnd_d(minMass,maxMass);
            object.X=rnd_d(minX,maxX);
            object.Y=rnd_d(minY,maxY);
            object.Z=rnd_d(minZ,maxZ);
            object.VelX=rnd_d(minVelX,maxVelX);
            object.VelY=rnd_d(minVelY,maxVelY);
            object.VelZ=rnd_d(minVelZ,maxVelZ);
            data.push_back(object);
        }
    }
};
#endif //HW2_RANDOMSETTINGS_H
