//
// Created by frycj on 29/12/2018.
//

#ifndef HW2_INPUTPARSER_H
#define HW2_INPUTPARSER_H

#include <sstream>
#include <vector>
#include <string>
#include <fstream>
#include "CosmicObjects.h"
#include "InputParser.h"
#include "RandomSettings.h"

class InputParser {
public:
    static std::vector <CosmicObject> readInstance(string instanceFileName) {
        vector <CosmicObject> objects;
        string line;

        ifstream file(instanceFileName);
        if (file.is_open()) {
            while (std::getline(file, line)) {
                objects.emplace_back(stringstream(line));
            }
            file.close();
        } else {
            throw runtime_error("It is not possible to open simulation file!\n");
        }
        return objects;
    }
    static std::vector<ObjectGenerator> readRandomGenerator(string fileName)
    {
        vector <ObjectGenerator> objects;
        string line;

        ifstream file(fileName);
        if (file.is_open()) {
            while (std::getline(file, line)) {
                objects.emplace_back(stringstream(line));
            }
            file.close();
        } else {
            throw runtime_error("It is not possible to open random generator file!\n");
        }
        return objects;
    }
    static InputSettings readConfiguration(string confFileName) {
        InputSettings settings;
        string line;
        ifstream file(confFileName);
        if (file.is_open()) {
            while (std::getline(file, line)) {
                stringstream ss(line);
                string name;
                ss>> name;
                if(name=="gravity")
                    ss >> settings.Gravity;
                else if(name=="timeFrom") {
                    ss >> settings.TimeFrom;
                    settings.TimeFrom*=24*60*60;
                }
                else if(name=="timeTo") {
                    ss >> settings.TimeTo;
                    settings.TimeTo*=24*60*60;
                }
                else if(name=="timeDelta") {
                    ss >> settings.TimeDelta;
                    settings.TimeDelta*=86400;
                }
                else if(name=="starsFixed") {
                    ss >> settings.StarsFixed;
                }
                else if(name=="planetsFixed") {
                    ss >> settings.PlanetsFixed;
                }
                else if(name=="singleThread") {
                    ss >> settings.SingleThread;
                }
                else if(name=="width")
                {
                    ss >> settings.FrameWidth;
                }
                else if(name=="height")
                {
                    ss >> settings.FrameHeight;
                }
                else if(name=="startX")
                {
                    ss >> settings.X1;
                }
                else if(name=="startY")
                {
                    ss >> settings.X2;
                }
                else if(name=="endX")
                {
                    ss >> settings.Y1;
                }
                else if(name=="endY")
                {
                    ss >> settings.Y2;
                }
            }
            file.close();
        } else {
            throw runtime_error("It is not possible to open simulation file!\n");
        }
        return settings;

    }
};

#endif //HW2_INPUTPARSER_H
