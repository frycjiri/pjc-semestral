//
// Created by frycj on 02/01/2019.
//

#ifndef HW2_SIMULATION_H
#define HW2_SIMULATION_H

#include <vector>
#include <string>
#include <omp.h>
#include <cmath>
#include <iostream>
#include "CosmicObjects.h"
#include "InputSettings.h"
#include "InputParser.h"
#include "gif.h"

class Simulation {
protected:
    uint8_t *createFrame(int frameWidth, int frameHeight) {
        return new uint8_t[frameWidth * frameHeight * 4];
    }

    void setPixel(
            uint8_t *frame,
            int frameWidth,
            int x,
            int y,
            uint8_t red,
            uint8_t green,
            uint8_t blue,
            uint8_t alpha) {
        int pixelIndex = 4 * (y * frameWidth + x);
        frame[pixelIndex + 0] = red;
        frame[pixelIndex + 1] = green;
        frame[pixelIndex + 2] = blue;
        frame[pixelIndex + 3] = alpha;
    }

    void fillFrame(
            uint8_t *frame, int frameWidth, int frameHeight,
            uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha) {
        for (int y = 0; y < frameHeight; y++) {
            for (int x = 0; x < frameWidth; x++) {
                setPixel(frame, frameWidth, x, y, red, green, blue, alpha);
            }
        }
    }

    void drawRectangle(
            uint8_t *frame, int frameWidth,
            int left, int top, int type) {
        int rectangleSize = type == 0 ? 1 : (type == 1 ? 3 : 5);
        rectangleSize = 1;
        for (int x = left; (x - left) < rectangleSize; x++) {
            for (int y = top; (y - top) < rectangleSize; y++) {
                if (x < 0 || y < 0 || x > frameWidth || y > frameWidth)
                    continue;
                setPixel(frame, frameWidth, x, y, (type == 0 || type == 2) ? 255 : 0,
                         (type == 2 || type == 1) ? 255 : 0, type == 0 ? 255 : 0, 255);
            }
        }
    }

    int translateX(float x, InputSettings settings) {
        return (int) ((x - settings.X1) / x_step);
    }

    int translateY(float y, InputSettings settings) {
        return (int) ((y - settings.Y1) / y_step);
    }

    float x_step, y_step;
    float Q_rsqrt( float number )
    {
        union {
            float f;
            uint32_t i;
        } conv{};

        float x2;
        const float threehalfs = 1.5f;

        x2 = number * 0.5f;
        conv.f  = number;
        conv.i  = 0x5f3759df - ( conv.i >> 1 );
        conv.f  = conv.f * ( threehalfs - ( x2 * conv.f * conv.f ) );
        return conv.f;
    }
};
#endif //HW2_SIMULATION_H
