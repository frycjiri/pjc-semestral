#include <utility>

//
// Created by frycj on 25/12/2018.
//

#ifndef PROJECT_NBODYSIMULATION_H
#define PROJECT_NBODYSIMULATION_H

#include <vector>
#include <string>
#include <omp.h>
#include <cmath>
#include <iostream>
#include "CosmicObjects.h"
#include "InputSettings.h"
#include "InputParser.h"
#include "gif.h"
#include "Simulation.h"

using namespace std;

class NNodySimulation : Simulation {

public:
    void render(InputSettings settings, string file, vector<CosmicObject> gravity_receivers) {

        string gifFile(std::move(file));
        uint32_t frameDelay = 0;
        GifWriter gifWriter{};
        x_step = (settings.X2 - settings.X1) / settings.FrameWidth;
        y_step = (settings.Y2 - settings.Y1) / settings.FrameHeight;
        GifBegin(&gifWriter, gifFile.c_str(), settings.FrameWidth, settings.FrameHeight, 0);
        auto frame = createFrame(settings.FrameWidth, settings.FrameHeight);

        float current_time = 0;
        float delta=settings.TimeDelta;
        float half_delta = settings.TimeDelta / 2;
        CosmicObject gravity_transmitters[gravity_receivers.size()];
#pragma omp parallel for simd
        for (int i = 0; i < gravity_receivers.size(); i++) {
            gravity_transmitters[i] = gravity_receivers[i];
            gravity_transmitters[i].Mass *=-settings.Gravity;
        }


        long receivers_count = gravity_receivers.size();

#pragma omp parallel for simd
        for (int i = 0; i < receivers_count; i++) {
            gravity_transmitters[i].X += gravity_transmitters[i].VelX * half_delta;
            gravity_transmitters[i].Y += gravity_transmitters[i].VelY * half_delta;
            gravity_transmitters[i].Z += gravity_transmitters[i].VelZ * half_delta;
        }
        while (current_time < settings.TimeTo) {
            fillFrame(frame, settings.FrameWidth, settings.FrameHeight, 0, 0, 0, 255);
#pragma omp parallel for simd collapse(2)
                for (int i = 0; i < receivers_count; i++) {
                    for (int j = 0; j < receivers_count; j++) {
                        if (gravity_transmitters[i].Id == gravity_transmitters[j].Id || gravity_transmitters[j].Mass >=0.0001)
                            continue;
                        float dX = gravity_transmitters[i].X - gravity_transmitters[j].X;
                        float dY = gravity_transmitters[i].Y - gravity_transmitters[j].Y;
                        float dZ = gravity_transmitters[i].Z - gravity_transmitters[j].Z;
                        float r = dX * dX + dY * dY + dZ * dZ;
                        float a=gravity_transmitters[j].Mass / r;
                        a = (a * Q_rsqrt(r))* delta;
                        gravity_transmitters[i].VelX += a*dX ;
                        gravity_transmitters[i].VelY += a*dY;
                        gravity_transmitters[i].VelZ += a*dZ;
                    }
                }
#pragma omp parallel for simd
                for (int i = 0; i < receivers_count; i++) {
                    gravity_transmitters[i].X += gravity_transmitters[i].VelX * delta;
                    gravity_transmitters[i].Y += gravity_transmitters[i].VelY * delta;
                    gravity_transmitters[i].Z += gravity_transmitters[i].VelZ * delta;
                    drawRectangle(frame, settings.FrameWidth, translateX(gravity_transmitters[i].X, settings),
                                  translateY(gravity_transmitters[i].Y, settings),
                                  gravity_transmitters[i].Type);
                }
        GifWriteFrame(&gifWriter, frame, settings.FrameWidth, settings.FrameHeight, frameDelay);
        current_time += settings.TimeDelta;
        }

    delete[]
    frame;
    GifEnd(&gifWriter);

}
};

#endif //PROJECT_NBODYSIMULATION_H
