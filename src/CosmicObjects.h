//
// Created by frycj on 25/12/2018.
//

#ifndef HW2_COSMICOBJECTS_H
#define HW2_COSMICOBJECTS_H

#include <string>
#include <sstream>
#include <random>

using namespace std;
class CosmicObject {
private:
    static int _id;
public:
    int Id=0;
    int Type=0;
    string Name;
    float Mass=1;
    float X=0,Y=0,Z=0;
    float VelX=1,VelY=1,VelZ=1;
    CosmicObject()
    {
        Id=++_id;
        Type=0;
        Name= "Asteroid";
    }
    explicit CosmicObject(stringstream ss)
    {
        Id=++_id;
        string T;
        ss >> T;
        if(T=="star")
            Type=2;
        else if(T=="planet")
            Type=1;
        else
            Type=0;
        ss >> Name;
        ss >> Mass >> X >> Y >> Z >> VelX >> VelY >> VelZ;
    }
};
int CosmicObject::_id=0;

#endif //HW2_COSMICOBJECTS_H
