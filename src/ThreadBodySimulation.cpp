//
// Created by frycj on 25/12/2018.
//

#include "ThreadBodySimulation.h"
#include <chrono>

void printHelpPage(char *program) {
    cout << "N-body simulation" << endl;
    cout << endl << "Usage:" << endl;
    cout << "\t" << program << " SIMULATION_PATH OUTPUT_PATH [GENERATOR_PATH]" << endl << endl;
}
using namespace std;
using namespace std::chrono;

int main(int argc, char **argv) {
    int c=0;
    string simulationFile;
    string outputFile;
    string randomFile;
    bool singleThread=false;
    for(int i=1;i<argc;i++)
    {
        if(argv[i][0]=='-')
        {
            if(strcmp(argv[i],"--help") == 0)
            {
                printHelpPage(argv[0]);
                return 0;
            }
            else if(strcmp(argv[i],"--single") == 0)
            {
                singleThread=true;
            }
            else
            {
                cout << "Unknown argument:" << argv[i] << endl;
                printHelpPage(argv[0]);
                return 0;
            }
        }
        else {
            if(c==0)
                simulationFile=argv[i];
            else if(c==1)
                outputFile=argv[i];
            else if(c==2)
                randomFile=argv[i];
            else{
                cout << "Too much arguments." << endl;
                printHelpPage(argv[0]);
                return 0;
            }
            c++;
        }
    }
    if(c<=1)
    {
        cout << "Missing arguments." << endl;
        printHelpPage(argv[0]);
        return 0;
    }
    InputSettings settings;
    vector<CosmicObject> objects;
    try {
        objects = InputParser::readInstance(simulationFile);
        if (c > 2) {
            vector<ObjectGenerator> gens = InputParser::readRandomGenerator(randomFile);
            for (ObjectGenerator &gen:gens) {
                gen.populate(objects);
            }
        }
    }
    catch(runtime_error &e)
    {
        cout << e.what();
        return 1;
    }
    PThreadBodySimulation body{};
    settings.SingleThread=singleThread;
    int NUM_CORE=1;
    if(singleThread)
    {
        NUM_CORE = 1;
    }
    else
        NUM_CORE = std::thread::hardware_concurrency();
    cout << "Using: "<< NUM_CORE << " threads." << endl;
    high_resolution_clock::time_point start = high_resolution_clock::now();
    body.render(settings, outputFile, objects,NUM_CORE);

    float totalDuration = duration_cast<duration<double>>(high_resolution_clock::now() - start).count();
    cout << "Computational time: " << totalDuration << " s" << endl;
}