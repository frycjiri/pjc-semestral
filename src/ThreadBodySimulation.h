//
// Created by frycj on 02/01/2019.
//

#ifndef HW2_PTHREADBODYSIMULATION_H
#define HW2_PTHREADBODYSIMULATION_H

#include <thread>
#include "Simulation.h"


class PThreadBodySimulation : Simulation {
private:
    InputSettings settings;
    float delta, half_delta;
    long receivers_count, offset, others;
    unsigned NUM_CORE;
    uint8_t* frame;
    vector<CosmicObject> gravity_receivers;

    void fullStep(int from, int to) {
        for (int i = from; i<to; i++) {
            gravity_receivers[i].X += gravity_receivers[i].VelX * delta;
            gravity_receivers[i].Y += gravity_receivers[i].VelY * delta;
            gravity_receivers[i].Z += gravity_receivers[i].VelZ * delta;
            drawRectangle(frame, settings.FrameWidth, translateX(gravity_receivers[i].X, settings),
                          translateY(gravity_receivers[i].Y, settings),
                          gravity_receivers[i].Type);
        }
    }

    void applyGravity(int from, int to) {
        for (int i = from; i<to; i++) {
            for (int j = 0; j < receivers_count; j++) {
                if (gravity_receivers[i].Id == gravity_receivers[j].Id || gravity_receivers[j].Mass >= 0.0001)
                    continue;
                float dX = gravity_receivers[i].X - gravity_receivers[j].X;
                float dY = gravity_receivers[i].Y - gravity_receivers[j].Y;
                float dZ = gravity_receivers[i].Z - gravity_receivers[j].Z;
                float r = dX * dX + dY * dY + dZ * dZ;
                float a = gravity_receivers[j].Mass / r;
                //r = (a / sqrt(r))* delta;
                a = (a * Q_rsqrt(r)) * delta;
                gravity_receivers[i].VelX += a * dX;
                gravity_receivers[i].VelY += a * dY;
                gravity_receivers[i].VelZ += a * dZ;
            }
        }
    }

    void init(int from, int to) {
        for (int i = from; i<to; i++) {
            gravity_receivers[i] = gravity_receivers[i];
            gravity_receivers[i].Mass *= -settings.Gravity;
            gravity_receivers[i].X += gravity_receivers[i].VelX * half_delta;
            gravity_receivers[i].Y += gravity_receivers[i].VelY * half_delta;
            gravity_receivers[i].Z += gravity_receivers[i].VelZ * half_delta;
        }
    }

    using job = void(PThreadBodySimulation::*)(int,int);
    void runJob(job fnc) {
        if (NUM_CORE > 1) {
            thread threads[NUM_CORE - 1];
            for (int i = 1; i < NUM_CORE; i++)
                threads[i - 1] = thread(fnc, this,others * i + offset, others * (i + 1) + offset);
            (this->*fnc)(0, others + offset);
            for (int i = 1; i < NUM_CORE; i++)
                threads[i-1].join();
        }
        else
            (this->*fnc)(0, others + offset);
    }

public:
    void render(InputSettings settings, string file, vector<CosmicObject> objects,int num_core) {
        gravity_receivers = objects;
        string gifFile(std::move(file));
        uint32_t frameDelay = 0;
        GifWriter gifWriter{};
        x_step = (settings.X2 - settings.X1) / settings.FrameWidth;
        y_step = (settings.Y2 - settings.Y1) / settings.FrameHeight;
        GifBegin(&gifWriter, gifFile.c_str(), settings.FrameWidth, settings.FrameHeight, 0);
        frame = createFrame(settings.FrameWidth, settings.FrameHeight);
        float current_time = 0;
        delta = settings.TimeDelta;
        half_delta = settings.TimeDelta / 2;


        receivers_count = gravity_receivers.size();
        this->NUM_CORE=num_core;
        offset = receivers_count % NUM_CORE;
        others = (receivers_count - offset) / NUM_CORE;

        runJob(&PThreadBodySimulation::init);

        while (current_time < settings.TimeTo) {
            fillFrame(frame, settings.FrameWidth, settings.FrameHeight, 0, 0, 0, 255);
            runJob(&PThreadBodySimulation::applyGravity);
            runJob(&PThreadBodySimulation::fullStep);
            GifWriteFrame(&gifWriter, frame, settings.FrameWidth, settings.FrameHeight, frameDelay);
            current_time += settings.TimeDelta;
        }

        delete[]
                frame;
        GifEnd(&gifWriter);

    }
};

#endif //HW2_PTHREADBODYSIMULATION_H
