cmake_minimum_required(VERSION 3.5)
project(NBodySimulation)

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Ofast -fopt-info-vec -std=c++11 -march=native")
endif()

add_executable(NBodySimulation src/ThreadBodySimulation.cpp)
target_link_libraries(NBodySimulation Threads::Threads)