#!/bin/bash

DATADIR="/storage/brno3-cerit/home/$LOGNAME/"

# module add pbspro-client
module add cmake-3.6.1
trap 'clean_scratch' TERM EXIT

cp -r $DATADIR/a/* $SCRATCHDIR || exit 1
cd $SCRATCHDIR || exit 2
dir

export OMP_NUM_THREADS=$PBS_NUM_PPN
cmake CMakeLists.txt
make
chmod 700 NBodySimulation
./NBodySimulation empty_space.csv output-${PBS_NUM_PPN}.gif basic.csv > results-${PBS_NUM_PPN}.txt

# Copy the results file to our home.
cp results-${PBS_NUM_PPN}.txt $DATADIR/out || export CLEAN_SCRATCH=false
cp output-${PBS_NUM_PPN}.gif $DATADIR/gif || export CLEAN_SCRATCH=false